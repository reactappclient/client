import React from "react";
import { Redirect } from "react-router";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Icon from '@material-ui/core/Icon';

const styles = ({
  icon: {
    marginRight: '15px',
    fontSize: 20,
  },
});


class TextFields extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      form: {
        login: '',
        password: '',
        email: '',
        avatar: ''
      },
      redirect: false,
    }
  }


  onChange = (event) => {

    const newState = {
      form: {
        ...this.state.form,
        [event.target.name]: event.target.value,
      }
    }
    this.setState(newState)
  }

  login = async () => {
    await fetch("http://localhost:8080/api/users", {
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        login: this.state.form.login,
        password: this.state.form.password,
        email: this.state.form.email,
        avatar: this.state.form.avatar,
      })
    }).then(res => res.json())
      .then(json => json.status === 'success' ? this.setState({ redirect: true }) : alert("Blad logowania"));
  };


  render() {
    if (this.state.redirect) {
      return <Redirect to="/login" />
    }

    return (
      <div className="formLogin">
        <form noValidate>
          <TextField
            label="login"
            name="login"
            margin="normal"
            variant="outlined"
            value={this.state.form.login}
            onChange={this.onChange}
            required
            fullWidth
          />

          <TextField
            label="password"
            name="password"
            margin="normal"
            variant="outlined"
            value={this.state.form.password}
            onChange={this.onChange}
            required
            fullWidth
          />
          <TextField
            label="email"
            name="email"
            margin="normal"
            variant="outlined"
            value={this.state.form.email}
            onChange={this.onChange}
            required
            fullWidth
          />
          <TextField
            label="avatar"
            name="avatar"
            margin="normal"
            variant="outlined"
            value={this.state.form.avatar}
            onChange={this.onChange}
            fullWidth
          />
          <Button variant="contained" color="primary" onClick={this.login} style={{ marginTop: '1em' }}>  <Icon style={styles.icon}> vpn_key </Icon>  Zarejestruj</Button>
        </form>
      </div>
    );
  }
}

export default TextFields;
