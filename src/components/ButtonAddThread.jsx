import React from 'react';
import Button from "@material-ui/core/Button";
import { Icon } from '@material-ui/core';
import { Link } from "react-router-dom";

function ButtonAddThread() {
  return (
    <div className="actionBar" style={{ textAlign: "right" }}>
      <Link to={`/NewThread`}>
        <Button variant="contained" color="primary" style={{ marginBottom: '1.5em' }}>
          Add thread <Icon> add </Icon>
        </Button>
      </Link>
    </div>
  );
}

export default ButtonAddThread;