import React from "react";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Icon from '@material-ui/core/Icon';

const styles = ({
  icon: {
    marginRight: '15px',
    fontSize: 20,
  },
});

class TextFields extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      form: {
        login: '',
        password: '',
      },
    }
  }


  onChange = (event) => {
    const newState = {
      form: {
        ...this.state.form,
        [event.target.name]: event.target.value,
      }
    }
    this.setState(newState)
  }

  login = async () => {
    await fetch("http://localhost:8080/api/login", {
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        login: this.state.form.login,
        password: this.state.form.password,
      })
    }).then(res => res.json())
      .then(json => this.logUser(json.data));
  };


  logUser = (user) => {
    this.props.logUser(user);
  }


  render() {
    return (
      <form>
        <TextField
          label="Name"
          name="login"
          margin="normal"
          variant="outlined"
          value={this.state.form.login}
          onChange={this.onChange}
          fullWidth
        />

        <TextField
          label="Password"
          type="password"
          name="password"
          margin="normal"
          variant="outlined"
          value={this.state.form.password}
          onChange={this.onChange}
          fullWidth
        />

        <Button variant="contained" color="primary" onClick={this.login} style={{ marginTop: '1em' }}>  <Icon style={styles.icon}> vpn_key </Icon>  Zaloguj</Button>
      </form>

    );
  }
}

export default TextFields;
