import React from "react";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import FormControl from "@material-ui/core/FormControl";
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';

const styles= {
  addComment: {
    marginTop: '1em',
    padding: '2em',
  }
}
class AddReplyForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      comment: '',
    }
  }

  sendComment = async () => {
    await fetch("http://localhost:8080/api/posts", {
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        id_user: this.props.user.id,
        id_thread: this.props.idThread,
        content: this.state.comment,
        date: new Date(),
      })
    }).then(res => res.json())
      .then(json => this.props.renderMessage(json.status));
  }

  onChange = (event) => {
    this.setState({ comment: event.target.value });
  }

  render() {
    return (
      <React.Fragment>
        <Typography variant="headline" component="h2"> Add Post </Typography>
        <Paper style={styles.addComment}>
          <FormControl fullWidth>
            <TextField
              multiline
              id="margin-normal"
              margin="normal"
              onChange={this.onChange}
            />
          </FormControl>
          <Button variant="contained" color="primary" onClick={this.sendComment}>Save Post</Button>
        </Paper>
      </React.Fragment>
    );
  }
};

export default AddReplyForm;
