import React, { Component } from "react";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";
import { Link } from "react-router-dom";
import moment from "moment";

const styles = ({
  a: {
    textDecoration: 'none',
    color: 'black',
  },
  cell: {
    id: {
      maxWidth: '30px',
    },
    count: {
      maxWidth: '100px',
    }
  }
});

export default class ThreadItem extends Component {
  render() {
    return (
      <TableRow>
        <TableCell style={styles.cell.id}>{this.props.item.id} </TableCell>
        <TableCell>
          <Link to={`/thread/${this.props.item.id}`} style={styles.a}>
            {this.props.item.title}
          </Link>
        </TableCell>
        <TableCell style={styles.cell.count} numeric> {this.props.item.count} </TableCell>
        <TableCell>
          {this.props.item.login} <br/>
          {moment(this.props.item.date).format("DD.MM.YYYY")}
        </TableCell>
      </TableRow>
    );
  }
}
