import React from "react";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import indigo from "@material-ui/core/colors/indigo";
import Button from "@material-ui/core/Button";
import { Link } from "react-router-dom";

const styles = ({
  banner: {
    background: indigo[600],
    marginBottom: '2em',
    padding: '2.5em',
  },
  text: {
    color: '#eee',
  },
  heading: {
    marginBottom: '1em',
    color: '#eee',
  },
  buttonContainer: {
    marginTop: '2em',
    button: {
      marginRight: '15px',
    }
  }
});

function PaperSheet(props) {

  return (
    <Paper style={styles.banner}>
      <Typography variant="headline" component="h1" style={styles.heading}>
        Welcome to Board Game
      </Typography>

      <Typography component="p" style={styles.text}>
        Quisque a ex et justo facilisis vulputate. Suspendisse bibendum erat eget libero rhoncus, in dictum lacus mollis. Mauris eu leo sed nisl interdum congue sodales non enim. Sed at feugiat ante, quis placerat lorem. Sed interdum erat nibh, non tempus massa sodales scelerisque. Nullam ac lacinia ante, eu faucibus augue. Sed vitae orci a mi bibendum posuere vitae in diam. Etiam lorem neque, mattis ac est ac, tristique hendrerit odio. Pellentesque facilisis enim ipsum, a imperdiet velit consequat sit amet. Nam eget augue at nisl semper dictum. Pellentesque aliquam massa dapibus luctus efficitur. Fusce congue, sem in ornare lacinia, lorem neque interdum nisl, sed rutrum metus sem a urna. Nullam malesuada ex ut urna commodo, molestie tincidunt nisi dapibus. Suspendisse fringilla dui at felis porttitor, vel molestie elit vestibulum. Donec fringilla elit vel elit pulvinar consequat vel vel ipsum. Nam ut sapien id ante accumsan dapibus.
      </Typography>

      <div style={styles.buttonContainer}>
        {props.user === null ?
          <Link to="/login">
            <Button variant="contained" color="primary" spacing={12} style={styles.buttonContainer.button}>
              logowanie
                </Button>
          </Link>
          :
          <Button variant="contained" color="primary" spacing={12} onClick={props.logout} style={styles.buttonContainer.button}>
            wyloguj
              </Button>
        }
        <Link to="/register">
          <Button variant="contained" color="primary" spacing={12}>
            rejestracja
            </Button>
        </Link>
      </div>
    </Paper>
  );
}

export default PaperSheet;
