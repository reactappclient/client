import React, { Component } from "react";
import { BrowserRouter, Route, Switch, Link } from "react-router-dom";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import MainBoard from "./containers/MainBoard";
import Thread from "./containers/Thread";
import Profile from "./containers/Profile";
import IconButton from "@material-ui/core/IconButton";
import AccountCircle from "@material-ui/icons/AccountCircle";
import Register from "./containers/Register";
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import NewThread from "./containers/NewThread";
import Login from "./containers/Login";
import "./App.css";

const PropsRoute = ({ component, ...rest }) => {
  return (
    <Route {...rest} render={routeProps => {
      return renderMergedProps(component, routeProps, rest);
    }} />
  );
}

const renderMergedProps = (component, ...rest) => {
  const finalProps = Object.assign({}, ...rest);
  return (
    React.createElement(component, finalProps)
  );
}

const styles = ({
  root: {
    flexGrow: 1,
    maxWidth: 1200,
    padding: '0 5%',
    margin: '0 auto',
  },
  a: {
    textDecoration: 'none',
  },
  appBar: {
    marginBottom: '2em',
  }
});

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: null,
    }
  }

  logUser = (user) => {
    if (user) {
      this.setState({ user });
    }
  };

  logout = () => {
    this.setState({ user: null });
  }


  render() {
    return (
      <Grid className="app" style={styles.root}>
        <BrowserRouter>
          <React.Fragment>
            <AppBar position="static" color="default" style={styles.appBar}>
              <Toolbar>
                <Link className="gameBoard" to="/" style={styles.a}>
                  <Typography variant="title" color="black" > Game Board </Typography>
                </Link>
                {this.state.user &&
                  <IconButton aria-haspopup="true" color="inherit" style={{ marginLeft: 'auto' }}>
                    <Link to={`/profile/${this.state.user.id}`}>
                      <AccountCircle />
                    </Link>
                  </IconButton>
                }
              </Toolbar>
            </AppBar>
            <Switch>
              <Route path="/profile/:id" component={Profile} />
              {this.state.user === null ? <PropsRoute path="/login" component={Login} logUser={this.logUser} /> : null}
              <Route path="/register" component={Register} />
              <PropsRoute path="/thread/:id" component={Thread} user={this.state.user} />
              {this.state.user ? <PropsRoute path="/newthread" component={NewThread} user={this.state.user} /> : null}
              <PropsRoute path="/" component={MainBoard} user={this.state.user} logout={this.logout} />
            </Switch>
          </React.Fragment>
        </BrowserRouter>
      </Grid>
    );
  }
}

export default App;
