import React, { Component } from "react";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import ThreadItem from "../components/ThreadItem";
import WelcomeBox from "../components/WelcomeBox";
import ButtonAddThread from "../components/ButtonAddThread";

const styles = ({
  a: {
    textDecoration: 'none',
    color: 'black',
  },
  cell: {
    id: {
      width: '0',
      maxWidth: '30px',
    },
    count: {
      maxWidth: '100px',
    }
  },
  mainBoard: {
    marginBottom: '2em',
  }
});

export default class MainBoard extends Component {
  constructor(props){
    super(props);
    this.state ={
      threads:[],
    } 
  }
  componentDidMount(){
    this.fetchThreads();
  } 
  fetchThreads = async() => {
   await fetch("http://localhost:8080/api/threads")
    .then(res=> res.json())
    .then(json=> this.setState({ threads: json.data }))
  }

  //this.setState({threads:json.data})
  render() {
   
    return (
      <React.Fragment>
        <WelcomeBox user={this.props.user} logout={this.props.logout} />
        {this.props.user && <ButtonAddThread /> }
        <Paper style={styles.mainBoard}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell style={styles.cell.id}>#</TableCell>
                <TableCell>Title</TableCell>
                <TableCell style={styles.cell.count} numeric>Posts</TableCell>
                <TableCell> Last Posts</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {this.state.threads.map(item => (
                <ThreadItem key={item.id} item={item}  />
              ))}
            </TableBody>
          </Table>
        </Paper>
      </React.Fragment>
    );
  }
}
