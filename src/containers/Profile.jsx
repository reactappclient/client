import React from "react";
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';

const styles = {
  profileCard: {
    maxWidth: '500px',
    margin: '2em auto',
  }
}

class TextFields extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      userData: [],
    }
  }

  componentDidMount() {
    this.fetchThreads();
  }

  fetchThreads = async () => {
    await fetch(`http://localhost:8080/api/users/${this.props.match.params.id}`)
      .then(res => res.json())
      .then(json => this.setState({ userData: json.data }))
  }

  render() {
    console.log(this.state.userData);
    return (
        <Card style={styles.profileCard}>
          <CardActionArea>
            <CardMedia
              component="img"
              alt={this.state.userData.login}
              height="240"
              image={this.state.userData.avatar}
              title={this.state.userData.login}
            />
            <CardContent>
              <Typography gutterBottom variant="headline" component="h2">
                {this.state.userData.login}
              </Typography>
              <Typography component="p">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse faucibus justo eget egestas rutrum. Nullam sollicitudin mauris eget vulputate aliquam. Integer fringilla leo eget lorem viverra, sit amet euismod est sodales. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nullam mollis id nulla eget hendrerit. Cras libero leo, efficitur et mauris sit amet, dignissim maximus sem. Suspendisse sagittis sagittis turpis nec semper. Duis elementum sem at magna blandit, non ultricies augue elementum. Nulla vel scelerisque tortor, sit amet pulvinar nisi. Nulla eleifend quam vitae est viverra blandit.
                Aenean purus metus, interdum consectetur condimentum mattis, vehicula et quam. Donec dignissim en
              </Typography>
            </CardContent>
          </CardActionArea>
        </Card>
    );
  }
}

export default TextFields;
