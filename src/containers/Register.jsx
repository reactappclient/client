import React, { Component } from 'react'
import RegisterForm from '../components/RegisterForm';
import Typography from '@material-ui/core/Typography';
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";

const styles = ({
  root: {
    flexGrow: 1,
    padding: '2em 0',
  },
  grid: {
    maxWidth: 600,
    margin: '0 auto',
  }
});

export default class Register extends Component {
  render() {
    return (
      <Paper style={styles.root} >
        <Grid container style={styles.grid}>
          <Typography variant="headline" component="h1">
            Rejestracja
          </Typography>
          <RegisterForm />
        </Grid>
      </Paper>
    )
  }
}
