import React, { Component } from "react";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import Avatar from "@material-ui/core/Avatar";
import CircularProgress from "@material-ui/core/CircularProgress";
import AddReplyForm from "../components/AddReplyForm";
import Snackbar from '@material-ui/core/Snackbar';
import { Link } from "react-router-dom";
import indigo from "@material-ui/core/colors/indigo";

const styles = ({
  titleBlock: {
    background: indigo[600],
    marginBottom: '2em',
    padding: '1em 2.5em',
  },
  text: {
    color: '#eee',
  },
  a: {
    width: '100%',
  },
  heading: {
    color: '#eee',
  },
  leftColumn: {
    width: '25%',
    padding: '1em',
    textAlign: 'center',
  },
  avatar: {
    width: '120px',
    height: '120px',
    marginLeft: 'auto',
    marginRight: 'auto',
    marginBottom: '0.5em',
  },
  comment: {
    margin: '0 0 1.5em ',
  }
});


export default class Thread extends Component {
  constructor(props) {
    super(props);
    this.state = {
      posts: [],
      thread: [],
      open: false,
      status: '',
    }
  }

  componentDidMount() {
    this.fetchThread();
    this.fetchPosts();
  }

  fetchThread = async () => {
    await fetch(`http://localhost:8080/api/threads/${this.props.match.params.id}`)
      .then(res => res.json())
      .then(json => this.setState({ thread: json.data }))
  }

  fetchPosts = async () => {
    await fetch(`http://localhost:8080/api/posts/${this.props.match.params.id}`)
      .then(res => res.json())
      .then(json => this.setState({ posts: json.data }))
  }

  renderMessage = (result) => {
    this.setState({ open: true, status: result })
    this.fetchPosts();
  }

  render() {
    if (this.state.posts.length < 1) {
      return <CircularProgress />;
    }
    if (this.state.posts.length < 1) {
      return <div>Brak komentarzy</div>;
    }

    return (
      <React.Fragment>
        <Paper style={styles.titleBlock}>
          <Typography variant="headline" component="h1" style={styles.heading}>
            {this.state.thread.title}
          </Typography>
        </Paper>
        
        {this.state.posts.map(post => (
          <Paper key={post.id} style={styles.comment}>
            <Grid container spacing={16}>
              <Grid item style={styles.leftColumn}>
                <Link to={`/profile/${post.userid}`} style={styles.a}>
                  <Avatar alt={post.login} src={post.avatar} style={styles.avatar}  />
                </Link>
                <div style={{ textAlign: 'center' }}>{post.login}</div>
              </Grid>
              <Grid item xs={12} sm container>
                <Grid item xs container direction="column" spacing={16}>
                  <Grid item xs>
                    <Typography gutterBottom variant="subheading">
                      {post.content}
                    </Typography>
                    <Typography gutterBottom />
                    <Typography color="textSecondary" />
                  </Grid>
                  <Grid item />
                </Grid>
                <Grid item>
                  <Typography variant="subheading" />
                </Grid>
              </Grid>
            </Grid>
          </Paper>
        ))}

        {this.props.user &&
          <AddReplyForm
            idThread={this.props.match.params.id}
            user={this.props.user}
            renderMessage={this.renderMessage}
          />}

        <Snackbar
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left',
          }}
          open={this.state.open}
          autoHideDuration={6000}
          message={<span id="message-id">{this.state.status}</span>}
        />
      </React.Fragment>
    );
  }
}
