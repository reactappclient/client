import React, { Component } from 'react';
import Typography from '@material-ui/core/Typography';
import LoginForm from '../components/LoginForm';
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";

const styles = ({
  root: {
    flexGrow: 1,
    padding: '2em 0',
  },
  grid: {
    maxWidth: 600,
    margin: '0 auto',
  }
});

export default class Login extends Component {
  render() {
    return (
      <Paper style={styles.root} >
        <Grid container style={styles.grid}>
          <Typography variant="headline" component="h1">
            Logowanie
        </Typography>
          <LoginForm logUser={this.props.logUser} test="test" />
        </Grid>
      </Paper>
    )
  }
}
