import React from "react";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Snackbar from "@material-ui/core/Snackbar";

class TextFields extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      title: '',
      comment: '',
      status: '',
      open: false,
    }
  }
  onChange = (event) => {
    this.setState({[event.target.name]: event.target.value});
  }

  addThread = async () => {
    await fetch("http://localhost:8080/api/threads", {
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        author: this.props.user.id,
        date: new Date(),
        title: this.state.title,
      })
    }).then(res => res.json())
    .then(json => this.addPost(json.data.id));
  }

  addPost = async (idThread) => {
    await fetch("http://localhost:8080/api/posts", {
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        id_user: this.props.user.id,
        id_thread: idThread,
        content: this.state.comment,
        date: new Date(),
      })
    }).then(res => res.json())
    .then(json => this.renderMessage(json.status));
  }

  renderMessage(result) {
    this.setState({open: true, status: result})
  }


  render() {
    return (
      <div className="formLogin" style={{ maxWidth:'600px', margin:'0px auto'}}>
        <h1>Dodaj nowy post</h1>
        <form  style={{ display: 'grid',textAalign:'center',margin:'0 auto' }}>
          <TextField
            name="title"
            type="text"
            label="Title"
            margin="normal"
            value={this.state.title}
            onChange={this.onChange}
          />
          
          <TextField
            name="comment"
            label="Content"
            multiline
            margin="normal"
            rows={8}
            value={this.state.comment}
            onChange={this.onChange}
          />

        </form>
        <Button variant="outlined" onClick={this.addThread}>Add</Button>
        <Snackbar
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left',
          }}
          open={this.state.open}
          autoHideDuration={6000}
          message={<span id="message-id">{this.state.status}</span>}
        />
      </div>
    );
  }
}

export default TextFields;
